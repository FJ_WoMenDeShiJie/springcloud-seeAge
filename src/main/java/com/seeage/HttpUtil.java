package com.seeage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author yujiang
 * @description 发送请求工具类
 * @date 2019/8/5 18:27
 */
public class HttpUtil {


    public static String seeName(String url) throws Exception {

        URL restURL = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) restURL.openConnection();
        conn.setRequestMethod("GET");
        conn.setDoOutput(true);
        conn.setAllowUserInteraction(false);
        BufferedReader bReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line, resultStr = "";
        while (null != (line = bReader.readLine())) {
            resultStr += line;
        }
        bReader.close();
        return resultStr;
    }

}
