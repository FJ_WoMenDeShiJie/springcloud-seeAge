package com.seeage;

import brave.sampler.Sampler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
public class SeeAgeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeeAgeApplication.class, args);
    }

    @RequestMapping("/seeName")
    public String seeName() throws Exception {
        return HttpUtil.seeName("http://localhost:8805/seeName");
    }

    @RequestMapping("/seeAge")
    public String seeAge() {
        return "16岁";
    }

}
